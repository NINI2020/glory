import Vue from 'vue'
import VueRouter from 'vue-router'
// import Home from '../views/Home.vue'
import Main from '../views/Main.vue'
import CategoryEdit from '../views/CategoryEdit.vue'
import CategoryList from '../views/CategoryList.vue'

import ItemEdit from '../views/ItemEdit.vue'
import ItemList from '../views/ItemList.vue'

import HeroEdit from '../views/HeroEdit.vue'
import HeroList from '../views/HeroList.vue'

import ArticleEdit from '../views/ArticleEdit.vue'
import ArticleList from '../views/ArticleList.vue'

import AdEdit from '../views/AdEdit.vue'
import AdList from '../views/AdList.vue'

import AdminUserEdit from '../views/AdminUserEdit.vue'
import AdminUserList from '../views/AdminUserList.vue'

import Login from '../views/Login.vue'

Vue.use(VueRouter)

const routes = [{
        path: '/login',
        name: 'login',
        component: Login,
        meta: {
            isPublic: true
        }
    },
    {
        path: '/',
        name: 'home',
        component: Main,
        children: [{
                path: '/categories/create',
                component: CategoryEdit
            },
            {
                path: '/categories/edit/:id',
                component: CategoryEdit,
                props: true
            },
            {
                path: '/categories/list',
                component: CategoryList
            },

            {
                path: '/items/edit/:id',
                component: ItemEdit,
                props: true
            },
            {
                path: '/items/list',
                component: ItemList
            },
            {
                path: '/items/create',
                component: ItemEdit
            },

            {
                path: '/heroes/edit/:id',
                component: HeroEdit,
                props: true
            },
            {
                path: '/heroes/list',
                component: HeroList
            },

            {
                path: '/heroes/create',
                component: HeroEdit
            },




            {
                path: '/articles/edit/:id',
                component: ArticleEdit,
                props: true
            },
            {
                path: '/articles/list',
                component: ArticleList
            },

            {
                path: '/articles/create',
                component: ArticleEdit
            },




            {
                path: '/ads/edit/:id',
                component: AdEdit,
                props: true
            },
            {
                path: '/ads/list',
                component: AdList
            },

            {
                path: '/ads/create',
                component: AdEdit
            },




            {
                path: '/admin_users/edit/:id',
                component: AdminUserEdit,
                props: true
            },
            {
                path: '/admin_users/list',
                component: AdminUserList
            },

            {
                path: '/admin_users/create',
                component: AdminUserEdit
            }

        ]
    },
    {
        path: '/about',
        name: 'about',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
            import ( /* webpackChunkName: "about" */ '../views/About.vue')
    }
]

const router = new VueRouter({
    routes
})

// to 要去的那个路由
// from 从哪里来
router.beforeEach((to, from, next) => {
    // console.log(to.meta)
    if (!to.meta.isPublic && !localStorage.token) {

        return next('/login')
    }
    next()
})
export default router