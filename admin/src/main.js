import Vue from 'vue'
import App from './App.vue'
import './plugins/element.js'
import router from './router'


Vue.config.productionTip = false

import http from './http'

//加载到原型之后,在任何页面就可以使用this.$http进行访问了
Vue.prototype.$http = http


Vue.mixin({
    computed: {
        uploadUrl() {
            return this.$http.defaults.baseURL + '/upload'
        }
    },
    methods: {
        getAuthHeaders() {
            return {
                Authorization: `bearer ${localStorage.token || ''}`
            }
        }
    }
})
new Vue({
    router,
    render: h => h(App)
}).$mount('#app')