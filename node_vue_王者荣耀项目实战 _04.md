## node_vue_王者荣耀项目实战 _04

#### 今日进度：

1. 英雄管理
2. 技能编辑
3. 文章管理
4. 富文本编辑 文章详情
5. 用户登陆密码加密
6. token验证
7. 必须要有token才能访问数据  
8. http-assert来处理判断后的不同的情况 
9. 前端还需要做路由校验：用户没有登录，但只要不请求接口的时候（比如创建分类界面），依然可以显示页面



###  英雄管理

![image-20191227100921072](C:\Users\tough\AppData\Roaming\Typora\typora-user-images\image-20191227100921072.png)

一个schema对象

里面有属性：name ,....

name:的值也是一个对象，有属性type



categories属性：值是一个数组，数组里面存着一个对象，为什么前面没有：像name:这样的属性名？

因为这是一个数组，数组哪里来的属性名？

数组没有属性一说

对象才有属性这么一说

 



object.assign()方法

### 富文本编辑文章详情 

* 在 admin下安装

![image-20200102112728780](C:\Users\tough\AppData\Roaming\Typora\typora-user-images\image-20200102112728780.png)

![image-20200102113505553](C:\Users\tough\AppData\Roaming\Typora\typora-user-images\image-20200102113505553.png)

**效果如下：🐂啊**

![image-20200102113428700](C:\Users\tough\AppData\Roaming\Typora\typora-user-images\image-20200102113428700.png)

### 用户登陆密码加密

使用 bcrypt 

在  server 目录下

npm install bcrypt -S  安装失败的话就安装  bcryptjs







###  token验证

在 server 下

npm install jsonwebtoken -S



### 必须要有token才能访问数据  

在 http.js中设置一个全局request 拦截器，加上请求头 token

```js
// Add a request interceptor
http.interceptors.request.use(function(config) {
    // Do something before request is sent
    config.headers.Authorization = localStorage.token
    return config;
}, function(error) {
    // Do something with request error
    return Promise.reject(error);
});
```

![image-20200104145809251](C:\Users\tough\AppData\Roaming\Typora\typora-user-images\image-20200104145809251.png)

然后就可以去后端的路由文件里面判断当前的路由请求头中有没有token,有执行命令，没有就

### http-assert包来处理判断后的不同的情况 

在 服务端 server中安装 

![image-20200104164632974](C:\Users\tough\AppData\Roaming\Typora\typora-user-images\image-20200104164632974.png)

可以用上面一行来替换下面的if判断

![image-20200104165442030](C:\Users\tough\AppData\Roaming\Typora\typora-user-images\image-20200104165442030.png)

默认就直接这样抛出错误，然后我们再用express的错误处理中间件处理即可

```js
 // 错误处理函数
    app.use(async(err, req, res, next) => {
        res.status(err.statusCode).send({
            message: err.message
        })
    })
```

如果没有token

```js
 assert(token, 401, "请提供jsonwebtoken格式的token")
```



![image-20200104171503734](C:\Users\tough\AppData\Roaming\Typora\typora-user-images\image-20200104171503734.png)





###  中间件文件里面如何访问app

![image-20200104195323178](C:\Users\tough\AppData\Roaming\Typora\typora-user-images\image-20200104195323178.png)

不能直接访问app. ,但是能够通过req.app....来使用  完全等价

### 前端还需要做路由校验：

用户没有登录，但只要不请求接口的时候（比如创建分类界面），依然可以显示页面

* **导航守卫**

```js
// to 要去的那个路由
// from 从哪里来
router.beforeEach((to, from, next) => {
    // console.log(to.meta)
    if (!to.meta.isPublic && !localStorage.token) {

        return next('/login')
    }
    next()
})
```

###  前端上传文件的地方登录校验

我觉得上传图片的地方可以i不用加登录校验，因为没有登录的话是没有办法进入其他页面的，所以我去掉了

###  一定记得使用中间件的时候要加个（） 



