# node_vue_moba

## Project setup
```
cd glory

npm init -y
```


### 进入到admin目录

```
cd admin

npm install 

npm run serve
```

### 进入到web目录
```
cd web

npm install 

npm run serve
```
### 进入到server目录

```
cd server

npm install 

npm run serve
```
### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
