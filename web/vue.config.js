module.exports = {
    lintOnSave: false,
    // 如果是生产环境下的话
    // 三元表达式
    // outputDir 输出到哪个文件夹,我们默认是输出到dist文件夹
    outputDir: __dirname + '/../server/web',
    // 没有使用publicPath 直接访问根路径即可 
    // publicPath: process.env.NODE_ENV === 'production' ?
    //     '/admin/' : '/'
}