import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './assets/scss/style.scss'
import './assets/iconfont/iconfont.css'

Vue.config.productionTip = false

import VueAwesomeSwiper from 'vue-awesome-swiper'

// require styles
import 'swiper/dist/css/swiper.css'

import Card from './components/Card.vue'
import ListCard from './components/ListCard.vue'
Vue.component('m-card', Card)
Vue.component('m-list-card', ListCard)
Vue.use(VueAwesomeSwiper, /* { default global options } */ )

import axios from 'axios'

Vue.prototype.$http = axios.create({
    baseURL: process.env.VUE_APP_API_URL || '/web/api'
        // baseURL: ' http://localhost:3000/web/api'

})

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')