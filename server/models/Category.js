var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// 这种具名化的文件就是专门用来创建独立的model的
var categorySchema = new Schema({
    name: {
        type: String,
        required: true
    },
    parent: {
        //表示是芒果数据库里面的一个id,千万不能设成字符串
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'Category' //所关联的模型
    }
})

//Creates a virtual type with the given name.
//创建名为Children的virtual type
// virtual 本身不会对mongoDB造成影响，只是作用于查询结果，
// children的值就是这样的一个对象：
// justOne 为true那就最多一条数据，如果为false就可以是一个数组。
// localFiled / foreignField  required for populate（填充） virtual 用来填充虚拟对象

// 什么时候用这个：
// 在使用populate关联查询的时候，需要满足两个条件其中一个：
// 1.是schema中的一个属性，有ref关联 
// 2. 如果不是一直需要这个属性，只是偶尔查询要，就可以使用virtual
// 
categorySchema.virtual('children', {
    localField: '_id', ////是这条article的id
    foreignField: 'parent',
    justOne: false,
    ref: 'Category'
})
categorySchema.virtual('newsList', {
    localField: '_id', //是这条article的id
    foreignField: 'categories',
    //由于关联的是article categories是Article的，在这里是外键
    justOne: false,
    ref: 'Article'
})


module.exports = mongoose.model('Category', categorySchema)