var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// 这种具名化的文件就是专门用来创建独立的model的


var AdSchema = new Schema({
    name: { type: String },
    // items表示一个广告位里面的items
    // 比如首页设置一个广告位做轮播图：
    // 那就items里面的一个item就包含了一张图片，这张图片还有一个链接
    items: [{
        image: { type: String },
        url: { type: String },
    }]
});

module.exports = mongoose.model('Ad', AdSchema)