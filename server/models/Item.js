var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// 这种具名化的文件就是专门用来创建独立的model的


var ItemSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    icon: {
        type: String
    }
});

module.exports = mongoose.model('Item', ItemSchema)