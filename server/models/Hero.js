var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// 这种具名化的文件就是专门用来创建独立的model的


var HeroSchema = new Schema({
    name: { type: String, required: true },
    avatar: { type: String },
    banner: { type: String },
    title: { type: String }, //比如亚瑟是圣骑之力
    categories: [{ type: mongoose.SchemaTypes.ObjectId, ref: 'Category' }], //因为一个英雄可能是法师又是刺客，属于多个分类
    scores: {
        difficulty: { type: Number }, //难度评分
        skills: { type: Number }, //技能评分
        attack: { type: Number }, //攻击力评分
        survive: { type: Number }, //生存能力评分
    },
    skills: [{
        icon: { type: String },
        name: { type: String },
        description: { type: String },
        tips: { type: String },
        delay: { type: String },
        cost: { type: String }
    }], //有多种技能，每种技能都有这些属性，所以是个对象

    // 如果把他写在[{equips1:{},equips2:{}}] 就表示一个装备同时具有这两个逆风和顺风，不符合逻辑
    equips1: [{ type: mongoose.SchemaTypes.ObjectId, ref: 'Item' }], //装备:顺风出装
    equips2: [{ type: mongoose.SchemaTypes.ObjectId, ref: 'Item' }], //装备：逆风出装
    usageTips: { type: String }, //使用技巧
    battleTips: { type: String }, //对抗技巧
    teamTips: { type: String }, //团战技巧
    partners: [{
        hero: { type: mongoose.SchemaTypes.ObjectId, ref: 'Hero' },
        description: { type: String }
    }],
});

// 由于 mongoDB 自动生成的 表名称 可能为 heros 但其实是heroes 为了确保一致，
// 我们在后面手动写上  heroes
module.exports = mongoose.model('Hero', HeroSchema, 'heroes')