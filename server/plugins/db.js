module.exports = app => {
    // 这个文件专门用来连接数据库
    const mongoose = require('mongoose');
    mongoose.connect('mongodb://localhost:27017/node-vue-moba', { useNewUrlParser: true });

    require('require-all')(__dirname + '/../models')
}